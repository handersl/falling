package han.app.falling;

import android.view.Window;
import android.view.WindowManager;
import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity 
{
	private GameView gameView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        gameView = new GameView(this, this);
        setContentView(gameView);
    }
    
    @Override
    public void onDestroy()
    {
    	super.onDestroy();
    	gameView.clean();
    	gameView = null;
    }
}
