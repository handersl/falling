package han.app.falling;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.Display;
import android.view.WindowManager;

public class Character 
{
	private String mCharacterName;
	private Bitmap mCharacterBitmap;
	private Rect mBoundingBox;
	private float mSpeed;
	
	//Screen stuff.
	private int mWidth;
	private int mHeight;
	
	//Character sizes.
	private static final int CATCHER_WIDTH = 108;
	private static final int CATCHER_HEIGHT = 192;
	private static final int FALLER_WIDTH = 192;
	private static final int FALLER_HEIGHT = 34;
	
	//Character stats.
	private static final float SPEED_ORIGINAL = 1f;
	private static final float SPEED_INC = .25f;
	private static final float FALLER_SPEED_MOD = .5f;
	
	public Character(Context context, String name)
	{
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		mWidth = size.x;
		mHeight = size.y;
		
		mCharacterName = name;
		if(name.equals("catcher"))
		{
			mCharacterBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.catching);
			mBoundingBox = new Rect(mWidth/2 - CATCHER_WIDTH/2, mHeight - CATCHER_HEIGHT, CATCHER_WIDTH, CATCHER_HEIGHT);
		}
		else
		{
			mCharacterBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.falling);
			mBoundingBox = new Rect(mWidth/2 - FALLER_WIDTH/2, 0, FALLER_WIDTH, FALLER_HEIGHT);
		}
		mSpeed = 1.0f;
	}
	
	public void update(double deltaTime, float controlValue)
	{
		if(mCharacterName.equals("catcher"))
		{
			mBoundingBox.offset((int) (mSpeed*deltaTime*controlValue), 0);
			if(mBoundingBox.left < 0 || mBoundingBox.left + CATCHER_WIDTH > mWidth)
			{
				mBoundingBox.offset((int) -(mSpeed*deltaTime*controlValue), 0);
			}
		}
		else
		{
			mBoundingBox.offset(0, (int) (mSpeed*deltaTime*FALLER_SPEED_MOD));
			
			if(mBoundingBox.top >= mHeight)
			{
				resetPositionAndIncreaseSpeed();
			}
		}
	}
	
	public void resetPositionAndIncreaseSpeed()
	{
		mSpeed += SPEED_INC;
		if(mCharacterName.equals("catcher"))
		{
			mBoundingBox = new Rect(mWidth/2 - CATCHER_WIDTH/2, mHeight - CATCHER_HEIGHT, CATCHER_WIDTH, CATCHER_HEIGHT);
		}
		else
		{
			if(mBoundingBox.top >= mHeight)
			{
				mSpeed = SPEED_ORIGINAL;
			}
			
			Random randomPositionGen = new Random();
			mBoundingBox = new Rect(randomPositionGen.nextInt(mWidth-FALLER_WIDTH), 0, FALLER_WIDTH, FALLER_HEIGHT);
		}
	}
	
	public void draw(Canvas canvas)
	{
		canvas.drawBitmap(mCharacterBitmap, mBoundingBox.left, mBoundingBox.top, null);
	}
	
	public void clean()
	{
		mCharacterBitmap.recycle();
		mCharacterBitmap = null;
		mBoundingBox = null;
	}
	
	public Rect getBoundingRect()
	{
		return mBoundingBox;
	}
}
