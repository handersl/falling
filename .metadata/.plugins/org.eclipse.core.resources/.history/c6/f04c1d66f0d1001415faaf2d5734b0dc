package han.app.falling;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;

//This is a class that checks the orientation sensors to find out
//how the phone is oriented.  Since the game only cares about one
//sensor this will only return the one sensor values the game needs.
public class OrientationController implements SensorEventListener
{
	private SensorManager mSensorManager;
	private Sensor mSensor;
	
	private Handler mCheckSensorHandler;
	private boolean mNeedToCheckSensor;
	private static final int MILLI_BETWEEN_SENSOR_CHECKS = 100;
	
	private float mControlSensorValue;

	public OrientationController(Activity activityToAttachSensorTo)
	{
		//This boolean will flip to false when a check is done and true when another check is due.
		mNeedToCheckSensor = true;
		//This handler will check the time periodically and flip the boolean above to tell the system to
		//check the sensor.
		mCheckSensorHandler = new Handler();
		mCheckSensorHandler.post(timeRunnable);
		
		//Grab the sensors.  There are warnings here but I think they are fine.
		mSensorManager = (SensorManager) activityToAttachSensorTo.getSystemService(Context.SENSOR_SERVICE);
		mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
		
		//Register the listener.
		mSensorManager.registerListener(this, mSensor, activityToAttachSensorTo.TRIM_MEMORY_MODERATE);
	
		mControlSensorValue = 0;
	}
	
	public void clean()
	{
		mCheckSensorHandler.removeCallbacksAndMessages(null);
		mCheckSensorHandler = null;
		
		mSensorManager.unregisterListener(this);
		mSensorManager = null;
		mSensor = null;
	}
	
	//We do the sensor checks here.
	@Override
	public void onSensorChanged(SensorEvent event) 
	{
		if(mNeedToCheckSensor)
		{
			mNeedToCheckSensor = false;
			
		    mControlSensorValue = axisX;
		    
		    //Schedule another check at the next time.
		    mCheckSensorHandler.postDelayed(timeRunnable, MILLI_BETWEEN_SENSOR_CHECKS);
		}
	}
		
	//Flips the check boolean to true to allow the sensor to check.
	Runnable timeRunnable = new Runnable() 
	{
        @Override public void run() 
        {
    		mNeedToCheckSensor = true;
        }
    };

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) 
	{	
	}
	
	public float getSensorValue()
	{
		return mControlSensorValue;
	}
}
