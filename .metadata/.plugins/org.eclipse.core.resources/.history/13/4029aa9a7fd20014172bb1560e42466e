package han.app.falling;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;

public class GameView extends View
{
	//Keep track of the previous time the game was updated.
	private long mLastTimeUpdated;
	
	private OrientationController mOrientationController;
	
	private Character mCatcher;
	private Character mFaller;
	
	private CloudOverlay mCloudOverlay;
	
	public GameView(Context context, Activity activityToAttachTo) 
	{
		super(context);
		
		this.setBackgroundResource(R.drawable.background);
		
		mOrientationController = new OrientationController(activityToAttachTo);
	
		mCatcher = new Character(context, "catcher");
		mFaller = new Character(context, "faller");
		
		mCloudOverlay = new CloudOverlay(context);
		
		mLastTimeUpdated = System.currentTimeMillis();
	}
	
	public void update()
	{
		mCatcher.update(System.currentTimeMillis() - mLastTimeUpdated, mOrientationController.getSensorValue());
		mFaller.update(System.currentTimeMillis() - mLastTimeUpdated, mOrientationController.getSensorValue());
		
		Log.d("Bounding", "Catcher " + mCatcher.getBoundingRect().left + " " + mCatcher.getBoundingRect().top + " " + mCatcher.getBoundingRect().width() + " " + mCatcher.getBoundingRect().height());
		Log.d("Bounding", "Faller " + mFaller.getBoundingRect().left + " " + mFaller.getBoundingRect().top + " " + mFaller.getBoundingRect().width() + " " + mFaller.getBoundingRect().height());
		
		if(Rect.intersects(mCatcher.getBoundingRect(), mFaller.getBoundingRect()))
		{
			mCatcher.resetPositionAndIncreaseSpeed();
			mFaller.resetPositionAndIncreaseSpeed();
			Log.d("Reset", "Resetting.");
		}
		
		if(mBoundingBox.top >= mScreenHeight)
		{
			resetPositionAndIncreaseSpeed();
		}
		
		mCloudOverlay.update(System.currentTimeMillis() - mLastTimeUpdated);
		
		mLastTimeUpdated = System.currentTimeMillis();
	}
	
	@Override
	public void onDraw(Canvas canvas)
	{
		update();
		
		super.onDraw(canvas);
		
		mCloudOverlay.draw(canvas);
		
		mFaller.draw(canvas);
		mCatcher.draw(canvas);
		
		invalidate();
	}

	public void clean()
	{
		mOrientationController.clean();
    	mOrientationController = null;
    	
    	mCatcher.clean();
    	mCatcher = null;
    	
    	mFaller.clean();
    	mFaller = null;
    	
    	mCloudOverlay.clean();
    	mCloudOverlay = null;
    }
}
