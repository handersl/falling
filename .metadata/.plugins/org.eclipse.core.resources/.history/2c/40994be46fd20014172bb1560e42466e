package han.app.falling;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;

public class GameView extends View
{
	//Keep track of the previous time the game was updated.
	private long mLastTimeUpdated;
	
	private OrientationController mOrientationController;
	
	private Character mCatcher;
	private Character mFaller;
	
	public GameView(Context context, Activity activityToAttachTo) 
	{
		super(context);
		
		this.setBackgroundResource(R.drawable.background);
		
		mOrientationController = new OrientationController(activityToAttachTo);
	
		mCatcher = new Character(context, "catcher");
		mFaller = new Character(context, "faller");
		
		mLastTimeUpdated = System.currentTimeMillis();
	}
	
	public void update()
	{
		mCatcher.update(System.currentTimeMillis() - mLastTimeUpdated, mOrientationController.getSensorValue());
		mFaller.update(System.currentTimeMillis() - mLastTimeUpdated, mOrientationController.getSensorValue());
		
		if(mCatcher.getBoundingRect().contains(mFaller.getBoundingRect()))
		{
			mCatcher.resetPositionAndIncreaseSpeed();
			mFaller.resetPositionAndIncreaseSpeed();
			Log.d("Reset", "Resetting.");
		}
		
		mLastTimeUpdated = System.currentTimeMillis();
	}
	
	@Override
	public void onDraw(Canvas canvas)
	{
		update();
		
		super.onDraw(canvas);
		
		mFaller.draw(canvas);
		mCatcher.draw(canvas);
		
		invalidate();
	}
	
	private boolean rectangleCollision(float x_1, float y_1, float width_1, float height_1, float x_2, float y_2, float width_2, float height_2)
	{
	  return !(x_1 > x_2+width_2 || x_1+width_1 < x_2 || y_1 > y_2+height_2 || y_1+height_1 < y_2);
	}
	
	public void clean()
	{
		mOrientationController.clean();
    	mOrientationController = null;
    	
    	mCatcher.clean();
    	mCatcher = null;
    	
    	mFaller.clean();
    	mFaller = null;
    }
}
